﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace SimpleServerCS
{
    class SimpleServer
    {
        TcpListener _tcpListener;

        public SimpleServer(bool UseLoopback, int port)
        {
            IPAddress ipAddress = UseLoopback ? IPAddress.Loopback : LocalIPAddress();
            _tcpListener = new TcpListener(ipAddress, port);
        }

        public void Start()
        {
            _tcpListener.Start();
            
            Console.WriteLine("Listening...");

            Socket socket = _tcpListener.AcceptSocket();
            Console.WriteLine("Connection Made");
            SocketMethod(socket);
        }

        public void Stop()
        {
            _tcpListener.Stop();
        }

        private void SocketMethod(Socket socket)
        {
            try
            {
                socket.Send(Encoding.UTF8.GetBytes("Send 0 for available options\n"));

                byte[] bytes = new byte[socket.ReceiveBufferSize];
                int k;
                while ((k = socket.Receive(bytes)) != 0)
                {
                    Console.WriteLine("Received...");

                    int i;
                    string receivedMessage = Encoding.UTF8.GetString(bytes, 0, k);

                    if (Int32.TryParse(receivedMessage, out i))
                    {
                        socket.Send(Encoding.UTF8.GetBytes(GetReturnMessage(i)));
                    }
                    else
                    {
                        socket.Send(Encoding.UTF8.GetBytes(GetReturnMessage(-1)));
                    }

                    if (i == 9)
                        break;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error occured: " + e.Message);
            }
            finally
            {
                socket.Close();
            }
        }

        private string GetReturnMessage(int code)
        {
            string returnMessage;

            switch (code)
            {
                case 0:
                    returnMessage = "Send 1, 3, 5 or 7 for a joke. Send 9 to close the connection.\n";
                    break;
                case 1:
                    returnMessage = "What dog can jump higher than a building? Send 2 for punchline!\n";
                    break;
                case 2:
                    returnMessage = "Any dog, buildings can't jump!\n";
                    break;
                case 3:
                    returnMessage = "When do Ducks wake up? Send 4 for punchline!\n";
                    break;
                case 4:
                    returnMessage = "At the Quack of Dawn!\n";
                    break;
                case 5:
                    returnMessage = "How do cows do mathematics? Send 6 for punchline!\n";
                    break;
                case 6:
                    returnMessage = "They use a cow-culator.\n";
                    break;
                case 7:
                    returnMessage = "How many programmers does it take to screw in a light bulb? Send 8 for punchline!\n";
                    break;
                case 8:
                    returnMessage = "None, that's a hardware problem.\n";
                    break;
                case 9:
                    returnMessage = "Bye!\n";
                    break;
                default:
                    returnMessage = "Invalid Selection\n";
                    break;
            }

            return returnMessage;
        }

        private IPAddress LocalIPAddress()
        {
            if (!System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
            {
                return null;
            }

            IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());

            return host
                .AddressList
                .LastOrDefault(ip => ip.AddressFamily == AddressFamily.InterNetwork);
        }

    }
}
